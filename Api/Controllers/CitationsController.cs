﻿using System;
using Microsoft.AspNetCore.Mvc;
using Repository.Models;
using Repository.Interfaces;
using System.Diagnostics;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CitationsController : Controller
    {
        private ICitationRepository _citationRepository;

        //dependency injection
        public CitationsController(ICitationRepository citationRepository)
        {
            _citationRepository = citationRepository;
        }

        // GET: api/Citations/5
        [HttpGet("{id}", Name = "GetCitation")]
        public IActionResult GetCitation(int id)
        {
            var citation = _citationRepository.GetCitation(id);

            if (citation == null)
            {
                return Ok();
            }

            return Ok(citation);
        }

        // UPDATE: api/Citations
        [HttpPut]
        public IActionResult UpdateCitation([FromBody] Citation citation)
        {
            try
            {
                if (citation == null)
                {
                    return BadRequest();
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                _citationRepository.UpdateCitation(citation);

                return CreatedAtRoute("GetCitation", new { id = citation.Id }, citation);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                throw;
            }
        }

        // POST: api/Citations
        [HttpPost]
        public IActionResult SaveCitation([FromBody] Citation citation)
        {
            try
            {
                if(citation == null)
                {
                    return BadRequest();
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                _citationRepository.SaveCitation(citation);

                return CreatedAtRoute("GetCitation", new { id = citation.Id }, citation);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                throw;
            }
        }
    }
}

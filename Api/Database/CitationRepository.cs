﻿using Repository.Interfaces;
using Repository.Models;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace Api.Database
{
    public class CitationRepository : ICitationRepository
    {
        private IDbConnection _dbConnection;

        public CitationRepository(IDbConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }

        //Gets a citation from database using stored procedure "GetCitation"
        public Citation GetCitation(int id)
        {
            SqlParameter idParameter = new SqlParameter();
            idParameter.ParameterName = "@Id";
            idParameter.SqlDbType = SqlDbType.Int;
            idParameter.Direction = ParameterDirection.Input;
            idParameter.Value = id;

            DbCommand command = new SqlCommand
            {
                CommandType = CommandType.StoredProcedure,
                CommandText = "GetCitation",
                Connection = new SqlConnection(_dbConnection.ConnectionString)
            };

            command.Parameters.Add(idParameter);
            command.Connection.Open();
            DbDataReader reader = command.ExecuteReader();

            Citation citation = new Citation();

            while (reader.Read())
            {
                citation.Id = reader.GetInt32(0);
                citation.CitationText = reader.GetString(1);
            }

            command.Connection.Close();

            return citation;
        }

        //Creates a citation at database through stored procedure called "SaveCitation"
        public void SaveCitation(Citation citation)
        {
            SqlParameter quoteParameter = new SqlParameter();
            quoteParameter.ParameterName = "@CitationText";
            quoteParameter.SqlDbType = SqlDbType.NVarChar;
            quoteParameter.Direction = ParameterDirection.Input;
            quoteParameter.Value = citation.CitationText;

            DbCommand command = new SqlCommand
            {
                CommandType = CommandType.StoredProcedure,
                CommandText = "SaveCitation",
                Connection = new SqlConnection(_dbConnection.ConnectionString)
            };

            command.Parameters.Add(quoteParameter);

            command.Connection.Open();
            command.ExecuteNonQuery();
            command.Connection.Close();
        }

        //updates a citation from database using stored procedure "UpdateCitation"
        public void UpdateCitation(Citation citation)
        {
            SqlParameter idParameter = new SqlParameter();
            idParameter.ParameterName = "@Id";
            idParameter.SqlDbType = SqlDbType.Int;
            idParameter.Direction = ParameterDirection.Input;
            idParameter.Value = citation.Id;

            SqlParameter quoteParameter = new SqlParameter();
            quoteParameter.ParameterName = "@CitationText";
            quoteParameter.SqlDbType = SqlDbType.NVarChar;
            quoteParameter.Direction = ParameterDirection.Input;
            quoteParameter.Value = citation.CitationText;

            DbCommand command = new SqlCommand
            {
                CommandType = CommandType.StoredProcedure,
                CommandText = "UpdateCitation",
                Connection = new SqlConnection(_dbConnection.ConnectionString)
            };

            command.Parameters.Add(idParameter);
            command.Parameters.Add(quoteParameter);

            command.Connection.Open();
            command.ExecuteNonQuery();
            command.Connection.Close();
        }
    }
}

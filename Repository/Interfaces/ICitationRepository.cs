﻿using Repository.Models;

namespace Repository.Interfaces
{
    public interface ICitationRepository
    {
        Citation GetCitation(int id);

        void SaveCitation(Citation book);
        void UpdateCitation(Citation book);
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace Repository.Models
{
    public class Citation
    {
        [Key]
        public int Id { get; set; }
        public string CitationText { get; set; }
    }
}

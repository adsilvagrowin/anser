- Website em .Net que tenha um interface web com um só campo de texto e um botão para gravar a informação do campo de texto na base de dados;
- Ao se aceder à página (acesso direto ou reload da página) o campo de texto deve ser preenchido com a informação gravada na base de dados, caso exista.
- Para gravar na base de dados vamos usar um webservice com a seguinte mecânica:
- Interface GUI envia os dados para o webservice;
- Webservice envia essa mesma informação para um (ou mais) Stored Procedure(s) SQL Server. Este(s) Stored Procedure(s) efetua(m) a(s) operação(ões) necessárias para Gravar os dados e Obter os dados.
- As camadas Web e Webservices são isoladas (websites diferentes) e apenas a camada dos Webservices acede à base de dados;
- Scripts de criação da Base de Dados, Tabela(s) e Stored Procedure(s), ou seja, nada de criação/modificação usando o Visual Studio ou o SQL Studio. Para podermos validar o exercício temos de conseguir criar tudo a partir de linha de comandos;
- Queremos tudo feito por código e quaisquer parâmetros guardados no web.config por forma a conseguir parametrizar tudo sem termos o Visual Studio ou o SQL Studio instalado no PC, por outra palavras, deve ser possível efetuar qualquer alteração usando o Notepad num PC que não tem qualquer ferramenta de desenvolvimento instalada.
- Todo o código deve ser comentado e indentado por forma a ter boa apresentação, incluindo os scripts SQL.
﻿namespace Web.Models
{
    public class Citation
    {
        public int Id { get; set; }
        public string CitationText { get; set; }
    }
}

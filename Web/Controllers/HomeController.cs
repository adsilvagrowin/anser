﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Web.Models;
using ApiClient;
using Microsoft.Extensions.Options;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly CoreApiClient apiClient;
        private readonly IOptions<Settings> _appSettings;
        private readonly Uri baseAddress;

        public HomeController(IOptions<Settings> appSettings)
        {
            _appSettings = appSettings;
            baseAddress = new Uri(_appSettings.Value.BaseAddress);
            apiClient = new CoreApiClient(baseAddress);
        }

        // Main Page from the web page. Shows a text area with a button
        public async Task<IActionResult> Index()
        {
            var requestUrl = apiClient.CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                $"citations/1"));

            var content = await apiClient.GetAsync<Citation>(requestUrl);


            if(content.CitationText != null)
            {
                ViewData["exists"] = "Update";
            }
            else
            {
                ViewData["exists"] = "Create";
            }

            return View(content);
        }

        // Action called when user clicks at the "Add Record" button
        [HttpPost]
        public async Task<IActionResult> Create([Bind("Id, CitationText")] Citation citation)
        {
            if (ModelState.IsValid)
            {
                var requestUrl = apiClient.CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                    $"citations/"));

                await apiClient.PostAsync<Citation>(requestUrl, citation);
                return RedirectToAction(nameof(Index));
            }

            return View(citation);
        }

        //Action called when a record already exists in database, so it can update that record
        [HttpPost]
        public async Task<IActionResult> Update([Bind("Id, CitationText")] Citation citation)
        {
            try
            {
                if (citation == null)
                {
                    return BadRequest();
                }

                if (ModelState.IsValid)
                {
                    var requestUrl = apiClient.CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                        $"citations/"));

                    await apiClient.PutAsync(requestUrl, citation);
                    return RedirectToAction(nameof(Index));
                }

                return View(citation);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                throw;
            }
        }

       
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

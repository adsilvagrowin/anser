﻿using Marvin.StreamExtensions;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ApiClient
{
    public class CoreApiClient
    {
        private readonly HttpClient _httpClient;
        private Uri BaseEndPoint { get; set; }

        public CoreApiClient(Uri baseEndPoint)
        {
            BaseEndPoint = baseEndPoint ?? throw new ArgumentException("baseEndPoint");
            _httpClient = new HttpClient();
        }

        public async Task<T> GetAsync<T>(Uri requestUrl)
        {
            var request = new HttpRequestMessage(
                HttpMethod.Get,
                requestUrl);

            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = await GetResponse<T>(request);

            return response;
        }

        public async Task<T> PostAsync<T>(Uri requestUrl, T content)
        {
            var memoryContentStream = new MemoryStream();
            memoryContentStream.SerializeToJsonAndWrite(content, new UTF8Encoding(), 1024, true);
            memoryContentStream.Seek(0, SeekOrigin.Begin);
            using (var request = new HttpRequestMessage(
                HttpMethod.Post,
                requestUrl))
            {
                request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                using (var streamContent = new StreamContent(memoryContentStream))
                {
                    request.Content = streamContent;
                    request.Content.Headers.ContentType =
                        new MediaTypeHeaderValue("application/json");

                    var response = await GetResponse<T>(request);
                    return response;
                }
            }
        }

        public async Task PutAsync<T>(Uri requestUrl, T content)
        {
            var response = await _httpClient.PutAsync(requestUrl.ToString(), CreateHttpContent<T>(content));
            response.EnsureSuccessStatusCode();
        }

        public Uri CreateRequestUri(string relativePath, string queryString = "")
        {
            var endpoint = new Uri(BaseEndPoint, relativePath);
            var uriBuilder = new UriBuilder(endpoint)
            {
                Query = queryString
            };
            return uriBuilder.Uri;
        }

        private static JsonSerializerSettings MicrosoftDateFormatSettings
        {
            get
            {
                return new JsonSerializerSettings
                {
                    DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
                };
            }
        }

        private HttpContent CreateHttpContent<T>(T content)
        {
            var json = JsonConvert.SerializeObject(content, MicrosoftDateFormatSettings);
            return new StringContent(json, Encoding.UTF8, "application/json");
        }

        private async Task<T> GetResponse<T>(HttpRequestMessage request)
        {
            using (var response = await _httpClient.SendAsync(request))
            {
                response.EnsureSuccessStatusCode();
                var stream = await response.Content.ReadAsStreamAsync();

                var content = stream.ReadAndDeserializeFromJson<T>();

                return content;
            }
        }
    }
}
